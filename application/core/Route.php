<?php

/**
 * Class Route
 */
class Route
{
    public static function start()
    {
        // default controller and action
        $controllerName = 'Main';
        $actionName = 'index';

        $routes = explode('/', $_SERVER['REQUEST_URI']);

        // get name of controller
        if (!empty($routes[1])) {
            $controllerName = ucfirst($routes[1]);
        }

        // name of action
        if (!empty($routes[2])) {
            $actionName = substr($routes[2], 0, strpos($routes[2], "?"));
        }

        // add prefix
        $modelName = $controllerName . 'Model';
        $controllerName = $controllerName . 'Controller';
        $actionName = $actionName . 'Action';

        // hook the file with the model class (model file may not be)
        $modelFile = $modelName . '.php';
        $modelPath = "application/models/" . $modelFile;
        if (file_exists($modelPath)) {
            include $modelPath;
        }

        // hook the file with the controller class
        $controllerFile = $controllerName . '.php';
        $controllerPath = "application/controllers/" . $controllerFile;

        if (file_exists($controllerPath)) {
            include $controllerPath;
        } else {
            Route::ErrorPage();
        }

        // create a controller
        $controller = new $controllerName;
        $action = $actionName;

        if (method_exists($controller, $action)) {
            // call action of controller
            $controller->$action();
        } else {
            // it would also be wiser to throw an exception here
            Route::ErrorPage();
        }
    }

    public static function ErrorPage()
    {
        $host = 'https://' . $_SERVER['HTTP_HOST'] . '/';

        header('Location:' . $host . 'error');
    }
}
