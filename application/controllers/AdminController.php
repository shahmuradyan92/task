<?php

class AdminController extends Controller
{
    /**
     * AdminController constructor.
     */
    public function __construct()
    {
        if (!isset($_SESSION['user'])) {
            header("Location: /login");
        }

        parent::__construct();

        $this->model = new AdminModel();
    }

    public function indexAction()
    {
        $data['success-message'] = '';
        $data['error-message'] = '';
        $data['content-error'] = '';

        $tasksCount = $this->model->getTasksCount();

        $data['pagination'] = true;

        if ($tasksCount < 3) {
            $data['pagination'] = false;
        }

        if ($_SERVER['REQUEST_METHOD'] === 'POST' && $_SESSION['user']) {
            $id = htmlspecialchars($_REQUEST['id'], ENT_QUOTES, 'UTF-8');
            $content = htmlspecialchars($_REQUEST['content'], ENT_QUOTES, 'UTF-8');

            if ($content === "") {
                $data['content-error'] = 'Content is required';
            } else {
                $sql = "UPDATE tasks SET content=?, updated_at=? WHERE id=?";
                $stmt = $this->model->pdo->prepare($sql);

                if ($stmt->execute([$content, date('Y-m-d H:i:s'), $id]) === true) {
                    $data['success-message'] = 'Task updated successfully';
                } else {
                    $data['error-message'] = 'Oops something went wrong! please try again.';
                }
            }
        }

        $this->view->generate('admin-view.php', $data);
    }

    public function jsonAction()
    {
        $columns = array(
            array('db' => 'id', 'dt' => 0),
            array('db' => 'name', 'dt' => 1),
            array('db' => 'email', 'dt' => 2),
            array('db' => 'content', 'dt' => 3),
            array('db' => 'status', 'dt' => 4),
            array('db' => 'updated_at', 'dt' => 5),
            array(
                'db' => 'id',
                'dt' => 6,
                'formatter' => function ($id) {
                    /** todo template must be in helper */
                    $buttonTemplate =
                        '<button 
                            data-id="%s"
                            
                            type="button"
                            class="btn btn-primary btn-sm" 
                            data-toggle="modal" 
                            data-target="#editTaskModal"
                        >
                            <i class="fas fa-edit"></i> Edit
                        </button>
                        <br>
                        <br>
                        <a href="/admin/status?id=%s" class="btn btn-success btn-sm">
                            <i class="fas fa-highlighter"></i> Set Done
                        </a>';
                    return sprintf($buttonTemplate, $id, $id);
                }
            ),
        );

        echo json_encode(
            $this->simple($_GET, $this->model->pdo, 'tasks', 'id', $columns)
        );
    }

    public function statusAction()
    {
        $id = htmlspecialchars($_GET['id'], ENT_QUOTES, 'UTF-8');

        $sql = "UPDATE tasks SET status=? WHERE id=?";
        $stmt = $this->model->pdo->prepare($sql);
        $stmt->execute(['Done', $id]);

        header("Location: /admin");
    }
}
