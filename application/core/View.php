<?php

class View
{
    /**
     * @param string $contentView
     * @param null $data
     */
	public function generate(string $contentView, $data = null)
	{
		include 'application/views/' . $contentView;
	}
}
