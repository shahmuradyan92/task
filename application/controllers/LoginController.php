<?php

class LoginController extends Controller
{
    /**
     * LoginController constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->model = new LoginModel();
    }

    public function indexAction()
    {
        $data['login-error'] = '';
        $data['password-error'] = '';
        $data['error-message'] = '';

        if (isset($_SESSION['user'])) {
            header("Location: /");
        }

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $login = $_REQUEST['login'];
            $password = $_REQUEST['password'];

            if ($login == '') {
                $data['login-error'] = 'Value is required';
            }

            if ($password == '') {
                $data['password-error'] = 'Value is required';
            }

            if ($data['login-error'] !== '' || $data['password-error'] !== '') {
                $this->view->generate('login-view.php', $data);

                exit();
            } else {
                $isUser = $this->model->isUser($login, $password);

                if ($isUser) {
                    $_SESSION['user'] = true;

                    header("Location: /admin");
                } else {
                    $data['error-message'] = 'Login or password is incorrect';
                }
            }
        }

        $this->view->generate('login-view.php', $data);
    }
}
