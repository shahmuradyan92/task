<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" type="text/css" href="/css/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css"/>

    <script src="/js/jquery-3.3.1.js"></script>
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>

    <script src="/js/datatables.min.js"></script>


    <title>Admin</title>
</head>
<body>

<?php /** @var $data */ ?>

<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-primary">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsDefault"
            aria-controls="navbarsDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsDefault">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/">Home</a>
            </li>
            <?php if (isset($_SESSION['user'])): ?>
                <li class="nav-item active">
                    <a class="nav-link" href="/admin">Admin</a>
                </li>
            <?php endif; ?>
        </ul>
        <div class="form-inline my-2 my-lg-0">
            <?php if (!isset($_SESSION['user'])): ?>
                <a href="/login" class="btn btn-outline-danger my-2 my-sm-0">Login</a>
            <?php else: ?>
                <a href="/logout" class="btn btn-outline-danger my-2 my-sm-0">Logout</a>
            <?php endif; ?>
        </div>
    </div>
</nav>

<main role="main">
    <div class="jumbotron">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="list-inline">
                        <li class="list-inline-item">
                            <h1>Admin</h1>
                        </li>
                    </ul>
                </div>
                <div class="col-md-12">
                    <table id="table-tasks" class="table table-bordered display w-100">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Content</th>
                            <th>Status</th>
                            <th>Updated at</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</main>

<div class="modal fade" id="editTaskModal" tabindex="-1" role="dialog" aria-labelledby="editTaskModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editTaskModalLabel">Edit Task</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="/admin">
                <div class="modal-body">
                    <div class="form-group">
                        <label readonly for="name">Name <span class="text-danger">*</span></label>
                        <input readonly type="text" class="form-control" id="name" name="name">
                    </div>
                    <div class="form-group">
                        <label for="email">Email address <span class="text-danger">*</span></label>
                        <input readonly type="email" class="form-control" id="email" name="email">
                    </div>
                    <div class="form-group">
                        <label for="content">Content <span class="text-danger">*</span></label>
                        <textarea type="text" class="form-control" id="content" name="content" required rows="6"></textarea>
                        <?php if (isset($data['content-error'])): ?>
                            <small class="form-text text-danger font-weight-bold"><?= $data['content-error'] ?></small>
                        <?php endif; ?>
                    </div>
                    <div class="form-group">
                        <input hidden type="text" class="form-control" id="id" name="id" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary font-weight-bold" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary font-weight-bold">Save task</button>
                </div>
            </form>
        </div>
    </div>
</div>

<?php if (isset($data['error-message'])): ?>
    <div class="modal fade" id="errorMessage" tabindex="-1" role="dialog" aria-labelledby="errorMessageLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <strong><?= $data['error-message'] ?></strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
    </div>
<?php endif; ?>

<?php if (isset($data['success-message'])): ?>
    <div class="modal fade" id="successMessage" tabindex="-1" role="dialog" aria-labelledby="successMessageLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong><?= $data['success-message'] ?></strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
    </div>
<?php endif; ?>

<script>
    $(document).ready(function () {
        $('#table-tasks').DataTable({
            processing: true,
            serverSide: true,
            order: [[1, 'asc']],
            lengthMenu: [3],
            bLengthChange: false,
            responsive: true,
            bPaginate: '<?= $data['pagination'] ?>',
            columns: [
                {
                    "bSortable": false,
                },
                {
                    "bSortable": true,
                },
                {
                    "bSortable": true,
                },
                {
                    "bSortable": false,
                },
                {
                    "bSortable": true,
                },
                {
                    "bSortable": false,
                },
                {
                    "bSortable": false,
                }
            ],
            ajax: {
                url: "/admin/json",
            },
        });

        $('#table-tasks').on('click', 'button', function (){
            let row = $(this).closest("tr");
            let name = row.find("td:eq(1)").text();
            let email = row.find("td:eq(2)").text();
            let content = row.find("td:eq(3)").text();

            $("form #id").val($(this).attr("data-id"));
            $("form #name").val(name);
            $("form #email").val(email);
            $("form #content").val(content);
        });

        if ('<?= $data['error-message'] ?>' !== "") {
            $('#errorMessage').modal('show')
        }

        if ('<?= $data['success-message'] ?>' !== "") {
            $('#successMessage').modal('show')
        }
    });
</script>
<script src="/js/popper.min.js"></script>
<script src="/js/bootstrap.min.js"></script>

</body>
</html>
