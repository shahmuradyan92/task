<?php

class AdminModel extends Model
{
    /**
     * @return int
     */
	public function getTasksCount(): int
    {
        $sql = "SELECT COUNT(*) as count FROM `tasks`";
        $query = $this->pdo->prepare($sql);
        $query->execute();

        $count = intval($query->fetch()['count']);

        return $count;
    }
}
