<?php
session_start();

require_once 'core/Model.php';
require_once 'core/View.php';
require_once 'core/Controller.php';

/*
Additional modules that implement various functionalities are usually connected here:
	> cashing
	> ORM
	> Unit test
	> Benchmarking
	> Backup
	> and etc.
*/

require_once 'core/Route.php';
Route::start(); // start routing
