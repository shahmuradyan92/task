<?php

class LogoutController extends Controller
{
    public function indexAction()
    {
        if (isset($_SESSION['user'])) {
            unset($_SESSION['user']);
            session_destroy();
            session_unset();
        }

        header('Location: /');
    }
}
