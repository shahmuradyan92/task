<?php

class LoginModel extends Model
{
    /**
     * @return array|void
     */
	public function getData()
	{
	    $data = [];

		return $data;
	}

    /**
     * @param string $login
     * @param string $password
     * @return bool
     */
	public function isUser(string $login, string $password): bool
    {
        $sql = "SELECT `password` FROM `users` WHERE `login` = '$login' LIMIT 1";

        $query = $this->pdo->prepare($sql);
        $query->execute();

        $passwordHash = $query->fetch()['password'];

        if (password_verify($password, $passwordHash)) {
            return true;
        } else {
            return false;
        }
    }
}
