<?php

class MainController extends Controller
{
    /**
     * MainController constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->model = new MainModel();
    }

    public function indexAction()
    {
        $data['success-message'] = '';
        $data['error-message'] = '';
        $data['name-error'] = '';
        $data['email-error'] = '';
        $data['content-error'] = '';

        $tasksCount = $this->model->getTasksCount();

        $data['pagination'] = true;

        if ($tasksCount < 3) {
            $data['pagination'] = false;
        }

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $name = htmlspecialchars($_REQUEST['name'], ENT_QUOTES, 'UTF-8');
            $email = htmlspecialchars($_REQUEST['email'], ENT_QUOTES, 'UTF-8');
            $content = htmlspecialchars($_REQUEST['content'], ENT_QUOTES, 'UTF-8');

            if ($name === "") {
                $data['name-error'] = 'Name is required';
            }

            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $data['email-error'] = "Invalid email format";
            }

            if ($content === "") {
                $data['content-error'] = 'Content is required';
            }

            if ($name !== "" && filter_var($email, FILTER_VALIDATE_EMAIL) && $content !== "") {
                $sql = "INSERT INTO `tasks` (`name`, `created_at`,`email`, `content`, `status`) VALUES (?,?,?,?,?)";
                $stmt = $this->model->pdo->prepare($sql);

                if ($stmt->execute([$name, date('Y-m-d H:i:s') , $email, $content, 'Note Done']) === true) {
                    $data['success-message'] = 'Task created successfully';
                } else {
                    $data['error-message'] = 'Oops something went wrong! please try again.';
                }
            }
        }

        $this->view->generate('main-view.php', $data);
    }

    public function jsonAction()
    {
        $columns = array(
            array('db' => 'id', 'dt' => 0),
            array('db' => 'name', 'dt' => 1),
            array('db' => 'email', 'dt' => 2),
            array('db' => 'content', 'dt' => 3),
            array('db' => 'status', 'dt' => 4),
            array('db' => 'updated_at', 'dt' => 5),
        );

        echo json_encode(
            $this->simple($_GET, $this->model->pdo, 'tasks', 'id', $columns)
        );
    }
}
