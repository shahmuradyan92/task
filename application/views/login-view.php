<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css"/>

    <script src="/js/jquery-3.3.1.js"></script>
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>


    <title>Login</title>
</head>
<body>

<?php /** @var $data */ ?>

<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-primary">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsDefault"
            aria-controls="navbarsDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsDefault">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/">Home</a>
            </li>
        </ul>
        <div class="form-inline my-2 my-lg-0">
            <a href="/login" class="btn btn-outline-danger my-2 my-sm-0">Login</a>
        </div>
    </div>
</nav>

<main role="main">
    <div class="jumbotron">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="list-inline">
                        <li class="list-inline-item">
                            <h1>Login</h1>
                        </li>
                    </ul>
                </div>
                <div class="offset-3 col-md-6">
                    <?php if ($data['error-message'] !== ''): ?>
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <strong><?= $data['error-message'] ?></strong>
                        </div>
                    <?php endif; ?>
                    <form action="/login" method="post">
                        <div class="form-group">
                            <label for="login">Login</label>
                            <input type="text" class="form-control" id="login" name="login">
                            <?php if (isset($data['login-error'])): ?>
                                <small class="form-text text-danger font-weight-bold"><?= $data['login-error'] ?></small>
                            <?php endif; ?>
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" id="password" name="password">
                            <?php if (isset($data['password-error'])): ?>
                                <small class="form-text text-danger font-weight-bold"><?= $data['password-error'] ?></small>
                            <?php endif; ?>
                        </div>
                        <button type="submit" class="btn btn-primary">Login</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</main>
<script src="/js/popper.min.js"></script>
<script src="/js/bootstrap.min.js"></script>

</body>
</html>
