<?php

class Model
{
    private $host = "localhost";
    private $username = "root";
    private $password = "123456";
    private $databaseName = "task";
    public $pdo;

    public function __construct()
    {
        if (!isset($this->pdo)) {
            try {
                $link = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->databaseName, $this->username, $this->password);
                $link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $link->exec("SET CHARACTER SET utf8");
                $this->pdo = $link;
            } catch (PDOException $exception) {
                die("Failed to connect with database " . $exception->getMessage());
            }
        }
    }
}